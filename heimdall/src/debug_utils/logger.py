import inspect
import logging
import resource
import psutil
from os import makedirs
from os.path import exists, isfile, split

def secure_limits():
    # Calculate secure memory parameters
    available_memory = psutil.virtual_memory().available  # (/1048576 for MB)
    secure_memory = int(available_memory / 8)
    return (available_memory, secure_memory)


# Create and configure logger
def setup_logger(logger_name, logfile, level=logging.DEBUG):
    func = inspect.currentframe().f_back.f_code
    logger = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s :::  %(message)s')
    folder, log_f = split(logfile)
    if exists(folder):
        fileHandler = logging.FileHandler(logfile, mode='a')
    else:
        makedirs(folder)
        fileHandler = logging.FileHandler(logfile, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)
    logger.setLevel(level)
    logger.addHandler(fileHandler)
    logger.addHandler(streamHandler)


def duration_log(logger_name, timer, notes = ""):
    logger = logging.getLogger(logger_name)
    message = "<TSPENT>{}:{}:{:.6f}</TSPENT> \t <N>{}</N>".format(timer.h, timer.m, timer.s, notes)
    logger.info(message)

def memory_log(logger_name, timer, notes = ""):
    logger = logging.getLogger(logger_name)
    func = inspect.currentframe().f_back.f_code
    message = "{}: {}: {} \t {} Mb {}".format(
        func.co_name,
        func.co_filename,
        func.co_firstlineno,
        int(resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024),
        message
    )
    logger.info(message)

def info_log(logger_name, message):
    logger = logging.getLogger(logger_name)
    func = inspect.currentframe().f_back.f_code
    message = "{}: {}: {} \t {}".format(
        func.co_name,
        func.co_filename,
        func.co_firstlineno,
        message 
    )
    logger.info(message)

def debug_log(logger_name, message):
    logger = logging.getLogger(logger_name)
    func = inspect.currentframe().f_back
    message = "{}: {}: {}:  \t {}".format(
        func.f_code.co_name,
        func.f_code.co_filename,
        func.f_code.co_firstlineno,
        message
    )
    logger.debug(message)
