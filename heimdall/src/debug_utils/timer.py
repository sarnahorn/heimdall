import time

class Timer():

    def __init__(self):
        self.start=time.time()
        self.ends = []

    def stop(self):
        self.ends.append(time.time())

    def duration(self):
        self.stop()
        self.time_lapse = self.ends[-1] - self.start
        self.h = int(self.time_lapse / 3600)
        self.m = int(self.time_lapse % 3600 / 60)
        self.s = self.time_lapse % 3600 % 3600

