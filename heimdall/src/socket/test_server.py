from socketserver import StreamRequestHandler, TCPServer
import pytest
import sys

class TestServer(StreamRequestHandler):
    
    respuesta = pytest.main(["-x","../test"])

    def handle(self):
        print("Se ha recibido una conexion desde {}".format(self.client_address))
        self.wfile.write(("{}".format(self.respuesta)).encode())

if __name__ == '__main__':
    servidor = TCPServer(('0.0.0.0', 5001), TestServer)
    print('Se ha iniciado el servidor.')
    servidor.serve_forever()
