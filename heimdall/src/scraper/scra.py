# -*- coding: utf-8 -*-
__author__ = 'RicardoMoya'

from unicodedata import normalize
from bs4 import BeautifulSoup
from os import path
from logging import getLogger 
from . import connection_manager as conmag
from debug_utils import logger


def scra(name, urlbuilder, url_base=""):
    logger.setup_logger("scrapper", f"/tmp/scrappe/{name}")
    LOGGER = getLogger("scrapper")
    LOGGER.info("scrapper runing")
    cm = conmag.ConnectionManager()
    urls = urlbuilder

    for i, url in enumerate(urls,1):
        LOGGER.info(f"esto entra: {url}")
        progress, href = url
        try:
            req = cm.request(f"{url_base}{href}")
        except BaseException as be:
            LOGGER.debug(be)
            LOGGER.info(f"<FAILID>{i}</FAILID>")
            LOGGER.info(f"<FAILURL>{href}</FAILURL>")
            continue

        status_code = req.code if req != '' else -1


        if status_code == 200:
            yield req
            LOGGER.info(f"{url} send to sopabella")
        else:
            LOGGER.info(f"<FAILID>{i}</FAILID>")
            LOGGER.info(f"<FAILURL>{href}</FAILURL>")
