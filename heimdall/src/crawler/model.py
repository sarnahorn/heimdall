import time
import os
from sqlalchemy import create_engine, exc
#from sqlalchemy import ReferencedTableError
from sqlalchemy import Table, Column, ForeignKey, MetaData
from sqlalchemy import String, Integer, Boolean
from sqlalchemy.orm import relationship, backref, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import database_exists, create_database
from debug_utils import logger
from sqlite3 import OperationalError

Base = declarative_base()
Session = sessionmaker()


domains2urls = Table("domains2urls",
                  Base.metadata,
                  Column("domain_id",
                         Integer,
                         ForeignKey("alloweddomains.domain_id"),
                         primary_key=True),
                  Column("url_id",
                         Integer,
                         ForeignKey("urls.url_id"),
                         primary_key=True),
                 )


urls2urls = Table("urls2urls",
                  Base.metadata,
                  Column("source_id",
                         Integer,
                         ForeignKey("urls.url_id"),
                         primary_key=True),
                  Column("url_id",
                         Integer,
                         ForeignKey("urls.url_id"),
                         primary_key=True),
)


class AllowedDomains(Base):
    __tablename__ = "alloweddomains"
    domain_id = Column(Integer, primary_key=True)
    domain = Column(String, unique=True)
    urls = relationship("Urls",
                        secondary="domains2urls",
                        backref=backref("domain_id"))


class Urls(Base):
    __tablename__ = "urls"
    url_id = Column(Integer, primary_key=True)
    url = Column(String, unique=True)
    status = Column(String)
    scraped = Column(Boolean)
    date = Column(String)
    domains = relationship("AllowedDomains",
                           secondary="domains2urls",
                           backref=backref("url_id"))
    urls = relationship("Urls",
                        secondary="urls2urls",
                        foreign_keys = [url_id],
                        backref=backref("url_id"))


class CrawlerDB():
    """ Modelo sqlalchemy para almacenar el proceso de obtención de urls)
    Attributes
    ----------
    """

    date = time.time()
    Urls = Urls()
    AllowedDomains = AllowDomains()
    Domains2Urls = Domains2Urls()
    Urls2Urls = Urls2Urls()

    def __init__(self, dbname,  dbfolder, initial_page, allow_domains):
        self.dbname = dbname
        self.dbfolder = dbfolder
        self.initial_page = initial_page
        self.db = f"{self.dbfolder}/{self.dbname}.db"
        self.engine = create_engine('sqlite:///' + self.db)
        logger.setup_logger(self.dbname,
                            "/tmp/heimdall/crawler/"+self.dbname+".log")
        if not database_exists(self.engine.url):
            try:
                Base.metadata.create_all(bind=self.engine)
            except exc.SQLAlchemyError as e:
                logger.debug_log(self.dbname, e)
        try:
            self.engine.connect()
            Session.configure(bind=self.engine)
            self.session = Session()
        except:
            logger.debug_log(self.dbname, "Session failed")

 
        """except exc.SQLAlchemyError as e:
            logger.debug_log(self.dbname, e)"""

