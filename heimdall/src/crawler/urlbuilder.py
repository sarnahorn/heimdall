# -*- coding: utf-8 -*-
import re
import csv
from logging import getLogger
from os import path
from debug_utils import logger

class UrlGenerator():

    """Generador de URL a partir de reglas o un listado  de entrada

    Attributes
    ----------
    name (str)    : Nombre de referencia para el objeto.
    base_url (str): La url de báse sobre la que se construyen las demás.
    state (str)   : Indica si el generador se acaba de inicializar.
        ("Initialized"), si se está procesando (Processing), si ha fallado.
        ("Error") o si finalizó satisfactoriamente ("finished").
    ready (list)  : Lista de elementos procesados
    generator (Callable[Any, str]): generator that produce last part of the  url
    processor (Callable[Any, bool]): Processor that indicates if the necesary
                    Action with the url are ready(true) or fail(false).
    """

    def __init__(self, name, base_url, generator, *args):
        self.name  = name
        self.base_url  = base_url
        self.state = "Initialized"
        self.ready = []
        self.process(self.processor, self.generator, *args)

    def rangebuilder(self, a, b, prefix="", start_by=1, skip_first=True):
        """Genera urls para  sitios con páginas numeradas
            
        Parameters
        ----------
        a


        """
        logger.setup_logger('rangebuilder', f'/tmp/urlbuilder/{name}')
        LOGGER = getLogger('rangebuilder')
        LOGGER.info('RangeBuilder inline')
        total_urls = b - a
        if start_by and a <= start_by <= b:
            a = start_by
        elif start_by:
            message = f"Prameter start_by, ({start_by}), is out of range {a},{b}"
            print(message)
            LOGGER.debug(message)
        for i in range(a, b + 1):
            remainig_urls = b - i
            percent = 100*i/total_urls
            try:
                if skip_first and i==1:
                    yield (percent, "")
                    LOGGER.info(f"Frist url (1) as void url parameter")
                else:
                    yield (percent, f"{prefix}{i}")
                    LOGGER.info(f"<READY>{i}</READY>")
            except:
                LOGGER.info("Someting is wrong with me, DOCUMENT ME BETTER ASSHOLE!!! ")
                LOGGER.info(f"This is only a hint... {i}:::skip_first->{skipfirst}")


    def     csvbuilder(name, filename, sep="\t", prefix="", start_by=1):
        logger.setup_logger('csvbuilder', f'/tmp/urlbuilder/{name}')
        LOGGER = getLogger('csvbuilder')                            
        if path.isfile(filename):
            LOGGER.info('CSVbuilder inline')
            size = path.getsize(filename)
            with open(filename,"r") as u:
                LOGGER.info(f"{filename} open")


                reader = csv.reader(u, delimiter=sep)
                readbytes = 0
                for row in reader:
                    readbytes += len(u"{}\t{}".format(row[0],row[1]).encode("utf-8"))
                    percent = round(readbytes/size)
                    if int(row[0]) > start_by:
                        yield (percent, f"{prefix}{row[1]}")
                        #LOGGER.info(f"<READY>{row[0]}</READY>")
                    

def last_ready(logfile):
    last_regex = re.compile(r"<READY>(\d+)</READY>")
    for line in reversed(list(open(logfile))):
        match = re.search(last_regex, line)
        if match:
            print(f"--------------------------->{match.groups()}")
            return match.groups()[0]

