from os import remove
from os.path import exists
from crawler.url_db import CrawlerDB


class TestClass(object):

    def test_crawlerdb(self):
        T = CrawlerDB("CrawlerTest", "/heimdall/outputs/tests/", "http://www.eltiempo.com", ["q","p","t"])
        assert exists("/heimdall/outputs/tests/CrawlerTest.db")

