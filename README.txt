
     ············································ .
     .                                            .
     .	H   H EEEEE IIIII MM MM DDD     A   L     .
     .	H   H E       I   MM MM D  D   A A  L     .
     .	HHHHH EEEE    I   M M M D   D AAAAA L     .
     .	H   H E       I   M   M D  D A    A L	  .
     .	H   H EEEEE IIIII M   M DDD A     A LLLLL .
     .                                            .
     ....................XXXXXX....................
                        XX    XX
                        | XXXX |                  
                        | XXXX |
                        XX    XX
		        |      |
     A scrap utility powered by Docker, Tor And Privoxy
                        |      |
                       ----------
                       ----------
                       ---------- 

  NOTES: 
    
    1)  There should be no privoxy or tor services 
        running on the host or they should be used 
        on different ports.

    2) To use pytest you must log in as heimdall.


  USAGE: 

  1) 
