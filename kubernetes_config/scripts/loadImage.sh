#!/bin/bash

kind load docker-image localhost:5000/heimdall_fog:1.0 --name heimdall-cluster --nodes heimdall-cluster-worker
kind load docker-image localhost:5000/heimdall_scraper:0.1 --name heimdall-cluster --nodes heimdall-cluster-worker
